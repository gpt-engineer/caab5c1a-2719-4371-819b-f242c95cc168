let timer;
let timeLeft;
let currentTimer;

document.getElementById('session').addEventListener('click', function() {
    startTimer(25);
});

document.getElementById('shortBreak').addEventListener('click', function() {
    startTimer(5);
});

document.getElementById('longBreak').addEventListener('click', function() {
    startTimer(15);
});

function startTimer(minutes) {
    if (timer) {
        clearInterval(timer);
    }

    timeLeft = minutes * 60;
    updateTimer();

    timer = setInterval(function() {
        timeLeft--;
        updateTimer();

        if (timeLeft <= 0) {
            clearInterval(timer);
            alert('Time is up!');
        }
    }, 1000);
}

function updateTimer() {
    let minutes = Math.floor(timeLeft / 60);
    let seconds = timeLeft % 60;

    if (seconds < 10) {
        seconds = '0' + seconds;
    }

    document.getElementById('timer').textContent = minutes + ':' + seconds;
}
